package com.company.optimizer;

import com.company.types.Matrix;
import com.company.types.Vec;

public class GD extends Optimizer{
    public GD(double learningRate){
        setLearningRate(learningRate);
    }
    @Override
    public Matrix updateWeights(Matrix weights, Matrix dCdW) {
        return weights.sub(dCdW.mul(getLearningRate()));
    }

    @Override
    public Vec updateBias(Vec bias, Vec dCdB) {
        return bias.sub(dCdB.mul(getLearningRate()));
    }
}
