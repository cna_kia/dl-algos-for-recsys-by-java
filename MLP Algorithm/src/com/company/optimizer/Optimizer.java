package com.company.optimizer;

import com.company.types.Matrix;
import com.company.types.Vec;

public abstract class Optimizer {
    private double learningRate;
    public abstract Matrix updateWeights(Matrix weights, Matrix dCdW);
    public abstract Vec updateBias(Vec bias, Vec dCdB);
    public void setLearningRate(double learningRate){
        this.learningRate = learningRate;
    }
    public double getLearningRate() {
        return learningRate;
    }
}
