package com.company.activation;

import static java.lang.StrictMath.exp;

public class Sigmoid extends Activation {

    @Override
    public double fn(double value) {
        return 1.0 / (1.0 + exp(-value));        // fn
    }

    @Override
    public double dFn(double param) {
        //Math.pow(Math.E, -param) / Math.pow(1 + Math.pow(Math.E, -value), 2)
        return this.fn(param) * (1.0 - this.fn(param));
    }

}
