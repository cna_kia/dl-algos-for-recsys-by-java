package com.company.activation;

import com.company.types.Vec;

import java.util.List;

public abstract class Activation {
    public Vec fn(Vec in){
        return in.map(this::fn);
    }
    public Vec dFn(Vec out){
        return out.map(this::dFn);
    }
    public Vec dCdI(Vec out, Vec dCdO) {
        return dCdO.mul(dFn(out));
    }
    protected abstract double fn(double val);
    protected abstract double dFn(double val);

}
