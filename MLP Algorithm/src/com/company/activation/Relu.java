package com.company.activation;

public class Relu extends Activation {
    @Override
    public double fn(double val) {
        return val > 0 ? val : 0;
    }

    @Override
    public double dFn(double param) {
        return param > 0 ? 1 : 0;
    }
}
