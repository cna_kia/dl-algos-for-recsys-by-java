package com.company.algorithm;

import com.company.core.Layer;
import com.company.initializer.Initializer;
import com.company.lossFunction.LossFucntion;
import com.company.optimizer.Optimizer;
import com.company.types.Matrix;
import com.company.types.Vec;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


//TODO: add model.predict() to Mlp
public class Mlp {
    private List<Layer> layers;
    private Optimizer optimizer;

    public Initializer getInitializer() {
        return initializer;
    }

    public void setInitializer(Initializer initializer) {
        this.initializer = initializer;
    }

    private LossFucntion lossFucntion;
    private double landa;
    private Initializer initializer;
    public Mlp(Initializer initializer, Layer... layers) {
        this.initializer = initializer;
        this.layers = new ArrayList<>();
        Layer l = layers[0];
        for (Layer layer : layers) {
            if (l == layer) {
                this.layers.add(layer);
                continue;
            }
            layer.setWeights(initializer.initWeights(l.getSize(), layer.getSize()));
            layer.initBias(l.getSize());
            layer.setPrecedingLayer(l);
            l = layer;
            this.layers.add(layer);
        }
    }

    public void addLayer(Layer... layers) {
        this.layers.addAll(Arrays.asList(layers));
    }

    private void forwardPropagate(Vec input) {
        Layer l;
        for (int count = 0; count <= layers.size() - 1; count++) {
            l = layers.get(count);
            if (count == 0) {
                l.setOut(input);
                continue;
            }
            input = l.calculateOutPut(input);
        }
    }

    private void backProp(Vec expected) {
        Layer layer = getLastLayer();
        /*
         d CostFn / d W = d Cost / d a * d a / d z * d z / d W;
         here we calculate costFn'.
         then inside the loop we calculate d a / d z * d z / d W;
         store it inside dCdO for next layer ;
         */
        Vec dCdO = lossFucntion.dFn(expected, layer.getOut());

        do {
            // calculating a' for each node in a layer and multiply by cost fn or last output
//            Vec dCdI = dCdO.mul(layer.getOut().map(x -> finalLayer.getActivation().dFn(x)));
            Vec dCdI = layer.getActivation().dCdI(layer.getOut(), dCdO);
            //dz / dw = x (Vector) => multiply x with dCdI to gain d cost fn / dw for present layer

            Matrix dCdW = dCdI.outerProduct(layer.getPrecedingLayer().getOut());

            layer.addDeltaWeightsAndBiases(dCdW, dCdI);
            dCdO = layer.getWeights().mul(dCdI);
            layer = layer.getPrecedingLayer();
        } while (layer.hasPrecedingLayer());

    }

    private void optimize() {
        layers.forEach(l -> {
            if (l.hasPrecedingLayer()) {
//                System.out.println(l.getDeltaWeightsAdded());
                if (landa > 0)
                    l.setWeights(l.getWeights().map(value -> value - (landa / l.getDeltaWeightsAdded()) * value));
                l.setWeights(optimizer.updateWeights(l.getWeights(), l.avgDw()));
                l.setBias(optimizer.updateBias(l.getBias(), l.avgBias()));
                l.cleanDeltaWeightsAndBias();
            }
        });
    }

    //TODO: add epoch to model.fit()
    public void fit(List<Vec> input, List<Vec> expected, int batch) {

        int count = -1;
        while (count++ < input.size() - 1) {

            forwardPropagate(input.get(count));
            backProp(expected.get(count));
            if (count != 0 && count % batch == 0 || count + 1 >= input.size())
                optimize();
        }

    }

    public double evaluate(List<Vec> input, List<Vec> expected) {
        int count = -1;
        var sum = 0.0;
        while (count++ < input.size() - 1) {
            forwardPropagate(input.get(count));
            sum += this.lossFucntion.fn(expected.get(count), getLastLayer().getOut());
        }
        return sum / count;
    }

    private Layer getLastLayer() {
        return layers.get(layers.size() - 1);
    }

    public void setLossFn(LossFucntion lossFn) {
        this.lossFucntion = lossFn;
    }

    public void setOptimizer(Optimizer optimizer) {
        this.optimizer = optimizer;
    }

    public void setLanda(double landa) {
        this.landa = landa;
    }
}
