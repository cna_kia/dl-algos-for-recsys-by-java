package com.company.core;

import com.company.activation.Activation;
import com.company.types.Matrix;
import com.company.types.Vec;


public class Layer {
    private final int size;
    private ThreadLocal<Vec> out = new ThreadLocal<>();
    private Matrix weights;
    private Vec bias;
    private Activation activation;
    private Layer precedingLayer;

    private Matrix deltaWeights;
    private Vec deltaBias;
    private int deltaWeightsAdded = 0;
    private int deltaBiasAdded = 0;

    public Layer(int size, Activation activation) {
        this.size = size;
        this.activation = activation;
    }

    public void initBias(int lastLayerSize) {
        var bias = new double[this.size];
        for (int j = 0; j < this.size; j++) {
            bias[j] = Math.random();
        }
        this.bias = new Vec(bias);
        this.deltaWeights = new Matrix(lastLayerSize, this.size);
        this.deltaBias = new Vec(this.size);
    }

    public boolean hasPrecedingLayer() {
        return precedingLayer != null;
    }


    public Vec Z(Vec input) {
        out.set(input.mul(weights).add(bias));
        return out.get();
    }

    public  Vec activate() {
        out.set(activation.fn(out.get()));
        return out.get();
    }

    public synchronized Vec calculateOutPut(Vec input) {
        this.Z(input);
        return this.activate();
    }

    public void addDeltaWeightsAndBiases(Matrix dW, Vec dB) {
        deltaWeights = deltaWeights.add(dW);
        deltaWeightsAdded++;
        deltaBias = deltaBias.add(dB);
        deltaBiasAdded++;
    }


    public void setPrecedingLayer(Layer precedingLayer) {
        this.precedingLayer = precedingLayer;
    }

    public Layer getPrecedingLayer() {
        return precedingLayer;
    }

    public Vec getOut() {
        return out.get();
    }

    public void setOut(Vec out) {
        this.out.set(out);
    }

    public Activation getActivation() {
        return activation;
    }

    public void cleanDeltaWeightsAndBias() {
        if (deltaWeightsAdded > 0) {
            deltaWeights.map(value -> 0);
            deltaWeightsAdded = 0;
        }
        if (deltaBiasAdded > 0) {
            deltaBias.map(value -> 0);
            deltaBiasAdded = 0;
        }
    }

    public Matrix getWeights() {
        return weights;
    }

    public Matrix avgDw() {
        return deltaWeights.map(x -> x / deltaWeightsAdded);
    }

    public Vec avgBias() {
        return deltaBias.map(x -> x / deltaBiasAdded);
    }

    public void setWeights(Matrix weights) {
        this.weights = new Matrix(weights.getData());
    }

    public void setBias(Vec bias) {
        this.bias = bias;
    }

    public Vec getBias() {
        return bias;
    }

    public int getSize() {
        return size;
    }

    public Matrix getDeltaWeights() {
        return deltaWeights;
    }

    public Vec getDeltaBias() {
        return deltaBias;
    }

    public int getDeltaWeightsAdded() {
        return deltaWeightsAdded;
    }

    public int getDeltaBiasAdded() {
        return deltaBiasAdded;
    }

    public String toString() {
        return String.format("bias vector size: %d\n weight Matrix size: %d*%d\ndelta bias vector size: %d\n delta weight Matrix size: %d*%d", bias.dimension(), weights.rows(), weights.cols(), deltaBias.dimension(), deltaWeights.rows(), deltaWeights.cols());
    }
}

