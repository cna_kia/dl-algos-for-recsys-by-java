package com.company.types;

import java.lang.reflect.Array;
import java.util.Arrays;

import static java.lang.String.format;
import static java.util.Arrays.stream;

public class Matrix {
    // i * j -> [i = rows][j = cols]
    private double[][] data;
    private int rows, cols;

    public Matrix(double[][] data) {
        this.data = data;
        rows = data.length;
        cols = data[0].length;
    }

    public Matrix(int rows, int cols) {
        this(new double[rows][cols]);
    }

    public int rows() {
        return rows;
    }

    public int cols() {
        return cols;
    }

    private void assertCorrectDimension(Matrix other) {
        if (rows != other.rows || cols != other.cols)
            throw new IllegalArgumentException(format("Matrix of different dim: Input is %d x %d, Vec is %d x %d", rows, cols, other.rows, other.cols));
    }

    private void assertCorrectDimensionOnMul(Matrix other) {
        if (cols != other.rows)
            throw new IllegalArgumentException(format("Matrix of different dim: Input is %d x %d, Vec is %d x %d", rows, cols, other.rows, other.cols));
    }

    public Matrix map(Function fn) {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                data[i][j] = fn.apply(data[i][j]);
            }
        }
        return this;
    }

    public Matrix add(Matrix matrix) {
        assertCorrectDimension(matrix);
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                data[i][j] += matrix.data[i][j];

        return this;
    }

    public Matrix sub(Matrix matrix) {
        assertCorrectDimension(matrix);
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                data[i][j] -= matrix.data[i][j];

        return this;
    }

    public Matrix mul(double s) {
        return map(value -> s * value);
    }

    public Vec mul(Vec v){
        double[] out = new double[rows];
        for (int y = 0; y < rows; y++)
            out[y] = new Vec(data[y]).dot(v);

        return new Vec(out);
    }

    public Matrix mul(Matrix matrix) {
        assertCorrectDimensionOnMul(matrix);
        double[][] result = new double[rows][matrix.cols];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < matrix.cols; j++)
                for (int k = 0; k < matrix.rows; k++)
                    result[i][j] += data[i][k] * matrix.data[k][j];

        return new Matrix(result);
    }

    public Matrix fillFrom(Matrix other) {
        assertCorrectDimension(other);

        for (int y = 0; y < rows; y++)
            if (cols >= 0) System.arraycopy(other.data[y], 0, data[y], 0, cols);

        return this;
    }

    public double average() {
        return stream(data).flatMapToDouble(Arrays::stream).average().getAsDouble();
    }

    public double variance() {
        double avg = average();
        return stream(data).flatMapToDouble(Arrays::stream).map(a -> (a - avg) * (a - avg)).average().getAsDouble();
    }

    public double[][] getData() {
        return data;
    }

    @Override
    public String toString() {
        return "Matrix{" +
                "data=" + Arrays.deepToString(data) +
                ", rows=" + rows +
                ", cols=" + cols +
                '}';
    }
}
