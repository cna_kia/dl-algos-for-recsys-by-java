package com.company.types;

@FunctionalInterface
public interface Function {
    double apply(double value);
}
