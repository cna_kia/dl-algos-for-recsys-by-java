package com.company.lossFunction;

import com.company.types.Vec;


public interface LossFucntion {
    double fn(double y, double y_hat);
    double fn(Vec y, Vec y_hat);
    Vec dFn(Vec y, Vec y_hat);
}
