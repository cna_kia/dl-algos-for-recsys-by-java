package com.company.lossFunction;

import com.company.types.Vec;

import static java.lang.Math.pow;

public class MSE implements LossFucntion {

    @Override
    public double fn(double y, double y_hat) {
        return pow(y - y_hat, 2);
    }

    @Override
    public double fn(Vec y, Vec y_hat) {
        Vec diff = y_hat.sub(y);
        return diff.dot(diff) / y.dimension();
    }

    @Override
    public Vec dFn(Vec y, Vec y_hat) {
        return y_hat.sub(y).mul(2.0);
    }

}
