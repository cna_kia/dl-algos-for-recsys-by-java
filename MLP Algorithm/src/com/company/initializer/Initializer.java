package com.company.initializer;

import com.company.types.Matrix;

public interface Initializer {
    Matrix initWeights(int row, int col);
}
