package com.company.initializer;

import com.company.types.Matrix;

import static com.company.initializer.SharedRnd.getRnd;

public class HenNormal implements Initializer {
    @Override
    public Matrix initWeights(int row, int col) {

        var matrix = new Matrix(row, col);
        final double factor = Math.sqrt(2.0 / matrix.cols());
        return matrix.map(value -> getRnd().nextGaussian() * factor);
    }
}
