package com.company.initializer;

import com.company.types.Matrix;

import static com.company.initializer.SharedRnd.getRnd;

public class XavierUniform implements Initializer {
    @Override
    public Matrix initWeights(int row, int col) {
        var matrix = new Matrix(row, col);
        final double factor = 2.0 * Math.sqrt(6.0 / (matrix.cols() + matrix.rows()));
        return matrix.map(value -> (getRnd().nextDouble() - 0.5) * factor);
    }
}
