package com.company.dataProvider;


import com.company.types.Vec;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class CsvReader {
    private List<List<String>> list;
    private String filePath;
    private String delimiter;

    public CsvReader(String filePAth, String delimiter) {
        list = new ArrayList<>();
        this.filePath = filePAth;
        this.delimiter = delimiter;
        this.read();
    }

    public void read() {
        try {
            list = new BufferedReader(new FileReader(filePath))
                    .lines()
                    .skip(1) //Skips the first n lines, in this case 1
                    .map(s -> getRecordFromLine(s))
                    .collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private List<String> getRecordFromLine(String line) {
        List<String> values = new ArrayList<String>();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(delimiter);
            while (rowScanner.hasNext()) {
                values.add(rowScanner.next());
            }
        }
        return values;
    }

    public void print(int lines) {
        int i = 0;
        while (i++ <= lines) {
            System.out.println(this.list.get(i));
        }
    }

    public List<Vec> vectorize() {
        List<Vec> result = new ArrayList<>();
        for (List<String> l : this.list) {
            double[] data = new double[l.size()];
            for (int i = 0; i < l.size() - 1; i++) {
                try {
                    data[i] = Double.parseDouble(l.get(i));
                } catch (NumberFormatException ex) {
                    data[i] = 0.0;
                }
            }
            result.add(new Vec(data));
        }
        return result;
    }

    public int rows() {
        return this.list.size();
    }
}
